# goMethodStats

Method execution statistics

## Getting started

```golang
import (
    "gitlab.com/dnpp_public/gomethodstats"
)
```

```golang
func main() {
    defer goMethodStats.GlobalMethodStats.PrintStats()
    ...
}

func main() {
    //TimeTrack.txt
    defer goMethodStats.GlobalMethodStats.PrintFileStats(&err)
    ...
}
```
```golang
func method1() {
    defer goMethodStats.TimeTrack(time.Now())
    ...
}

func method2() {
    defer goMethodStats.TimeTrack(time.Now())
...
}
```
