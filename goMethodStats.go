package goMethodStats

import (
	"fmt"
	"os"
	"runtime"
	"sync"
	"time"
)

type methodStats struct {
	mu    sync.Mutex
	stats map[string][]time.Duration
}

var GlobalMethodStats = methodStats{
	stats: make(map[string][]time.Duration),
}

func (ms *methodStats) add(functionName string, duration time.Duration) {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	ms.stats[functionName] = append(ms.stats[functionName], duration)
}

func (ms *methodStats) PrintStats() {
	ms.mu.Lock()
	defer ms.mu.Unlock()
	for functionName, durations := range ms.stats {
		var total time.Duration
		for _, duration := range durations {
			total += duration
		}
		average := total / time.Duration(len(durations))
		fmt.Printf("%s: %d invocations, average time %s\n", functionName, len(durations), average)
	}
}

func (ms *methodStats) PrintFileStats(err *error) {
	ms.mu.Lock()
	defer ms.mu.Unlock()

	fileName := `TimeTrack.txt`

	str := ""
	for functionName, durations := range ms.stats {
		var total time.Duration
		for _, duration := range durations {
			total += duration
		}
		average := total / time.Duration(len(durations))

		str += fmt.Sprintf("%s: %d invocations, average time %s\n", functionName, len(durations), average)
	}

	*err = os.WriteFile(fileName, []byte(str), 0644)
}

func TimeTrack(start time.Time) {
	pc, _, _, _ := runtime.Caller(1)
	methodName := runtime.FuncForPC(pc).Name()
	elapsed := time.Since(start)
	GlobalMethodStats.add(methodName, elapsed)
}
